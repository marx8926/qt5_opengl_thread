#include "renderthread.h"
#include "lidarwidget.h"

RenderThread::RenderThread(LidarWidget *parent)
{
    this->mlidarwidget = parent;
    this->dorendering = true;

}


RenderThread::~RenderThread()
{

}

void RenderThread::stop()
{
    this->dorendering = false;

}


void RenderThread::run()
{
    qDebug() << "RenderThread " << QThread::currentThreadId();
    mlidarwidget->makeCurrent();
    initGL();
    forever
    {
        if(!dorendering)
        {
            return;
        }

        draw();
        this->mlidarwidget->swapBuffers();
    }
    mlidarwidget->doneCurrent();
}

void RenderThread::initGL()
{
    mlidarwidget->qglClearColor(Qt::black);

    glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(-2.0, 2.0, -2.0, 2.0, 1.0, 15.0);
        glMatrixMode(GL_MODELVIEW);
        glViewport(0, 0, 400, 400);
        glClearColor(0.0, 0.0, 0.0, 1.0);

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glShadeModel(GL_SMOOTH);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);

        static GLfloat lightPosition[4] = { 0, 0, 10, 1.0 };
        glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

void RenderThread::draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        glTranslatef(0.0, 0.0, -10.0);

        /*
    QMatrix4x4 matrix;
    matrix.translate(0.0, 0.0, -5.0);
    matrix.rotate(rotation);
     glLoadIdentity();
*/
    glColor3f(1,0,0);
      glBegin(GL_QUADS);
          glNormal3f(0,0,-1);
          glVertex3f(-1,-1,0);
          glVertex3f(-1,1,0);
          glVertex3f(1,1,0);
          glVertex3f(1,-1,0);

      glEnd();
      glBegin(GL_TRIANGLES);
          glNormal3f(0,-1,0.707);
          glVertex3f(-1,-1,0);
          glVertex3f(1,-1,0);
          glVertex3f(0,0,1.2);
      glEnd();
      glBegin(GL_TRIANGLES);
          glNormal3f(1,0, 0.707);
          glVertex3f(1,-1,0);
          glVertex3f(1,1,0);
          glVertex3f(0,0,1.2);
      glEnd();
      glBegin(GL_TRIANGLES);
          glNormal3f(0,1,0.707);
          glVertex3f(1,1,0);
          glVertex3f(-1,1,0);
          glVertex3f(0,0,1.2);
      glEnd();
      glBegin(GL_TRIANGLES);
          glNormal3f(-1,0,0.707);
          glVertex3f(-1,1,0);
          glVertex3f(-1,-1,0);
          glVertex3f(0,0,1.2);
      glEnd();


}

void RenderThread::resizeGL(int width, int height)
{
    /*
    // Set OpenGL viewport to cover whole widget
    glViewport(0, 0, w, h);

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    const qreal zNear = 3.0, zFar = 7.0, fov = 45.0;

    // Reset projection
    projection.setToIdentity();

    // Set perspective projection
    projection.perspective(fov, aspect, zNear, zFar);
    */

    int side = qMin(width, height);
       glViewport((width - side) / 2, (height - side) / 2, side, side);

       glMatrixMode(GL_PROJECTION);
       glLoadIdentity();
   #ifdef QT_OPENGL_ES_1
       glOrthof(-2, +2, -2, +2, 1.0, 15.0);
   #else
       glOrtho(-2, +2, -2, +2, 1.0, 15.0);
   #endif
       glMatrixMode(GL_MODELVIEW);
}
