#ifndef LIDARWIDGET_H
#define LIDARWIDGET_H

#include <QGLWidget>
#include <QGLFunctions>
#include <QtOpenGL>
#include <QThread>
#include "renderthread.h"

class LidarWidget: public QGLWidget, protected QGLFunctions
{
    public:
        explicit LidarWidget(QWidget * parent);
        ~LidarWidget();

        void startRendering();
        void stopRendering();

    private:
        void resizeEvent(QResizeEvent *event);
        void paintEvent(QPaintEvent *);

        RenderThread thread;

};

#endif // LIDARWIDGET_H
