#ifndef RENDERTHREAD_H
#define RENDERTHREAD_H

#include <QThread>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QtOpenGL>

class LidarWidget;

class RenderThread : public QThread
{
    Q_OBJECT
public:
    explicit RenderThread(LidarWidget * lid);
    ~RenderThread();

    void stop();
    void resizeGL(int w, int h);

protected:
    void run();

    void initGL();
    void draw();
    LidarWidget * mlidarwidget;
    bool dorendering;

    QMatrix4x4 projection;
    QQuaternion rotation;
    QVector3D rotationAxis;
    qreal angularSpeed;




};

#endif // RENDERTHREAD_H
