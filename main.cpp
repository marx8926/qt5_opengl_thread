#include <QApplication>
#include <QWindow>
#include <QThread>
#include <QDebug>
#include "lidarwidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LidarWidget * w = new LidarWidget(0);

    w->show();
    w->startRendering();

    return a.exec();
}
