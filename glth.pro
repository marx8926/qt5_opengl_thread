#-------------------------------------------------
#
# Project created by QtCreator 2014-11-20T17:37:10
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = glth
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    renderthread.cpp \
    lidarwidget.cpp

HEADERS += \
    renderthread.h \
    lidarwidget.h
