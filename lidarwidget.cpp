#include "lidarwidget.h"

LidarWidget::LidarWidget(QWidget * parent): QGLWidget(parent), thread(this)
{
    setMinimumSize(400, 400);
    setAutoBufferSwap(true);
    resize(400,400);
}

LidarWidget::~LidarWidget()
{

}

void LidarWidget::startRendering()
{
    qDebug() << "GUIThread " << QThread::currentThreadId();
    doneCurrent();
    context()->moveToThread(&thread);
    thread.start();

}
void LidarWidget::stopRendering()
{
    thread.stop();
    thread.wait();

}
void LidarWidget::resizeEvent(QResizeEvent *event)
{
    thread.resizeGL(event->size().width(), event->size().height());

}

void LidarWidget::paintEvent(QPaintEvent *event)
{

}
